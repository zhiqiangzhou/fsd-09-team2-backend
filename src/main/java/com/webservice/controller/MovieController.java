package com.webservice.controller;

import com.webservice.entity.Movie;
import com.webservice.service.MovieService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class MovieController {


    @Autowired
    private MovieService movieService;

    @Data
    public static class MovieDTO{
        private MultipartFile image;
        private String title;
        private String genre;
        private Integer year;
        private Float rating;
        private String director;
        private String cast;
    }

    @GetMapping("/getAllMovies")
    public ResponseEntity<?> getAllMovies(){
        List<Movie> movieList = movieService.getAllMovies();
        return ResponseEntity.ok(movieList);
    }

    @GetMapping("/test")
    public ResponseEntity<?> getData(){
        return ResponseEntity.ok("AAA");
    }
    @DeleteMapping("/deleteMovie/{movieId}")
    public ResponseEntity<?> deleteMovie(@PathVariable Long movieId){
        Boolean result = movieService.deleteMovie(movieId);
        if (result){
            return ResponseEntity.ok("deleteMovie successful");
        }else {
            return ResponseEntity.ok("deleteMovie failed");
        }
    }

    @PutMapping("/updateMovie/{movieId}")
    public ResponseEntity<?> updateMovie(@PathVariable Long movieId, @ModelAttribute MovieDTO movieDTO){
        Boolean result = movieService.updateMovie(movieId,movieDTO);
        if (result){
            return ResponseEntity.ok("updateMovie successful");
        }else {
            return ResponseEntity.ok("updateMovie failed");
        }
    }


    @PostMapping("/createMovie")
    public ResponseEntity<?> createMovie(@ModelAttribute MovieDTO movie) {
        Boolean result = movieService.createMovie(movie);
        if (result){
            return ResponseEntity.ok("createMovie successful");
        }else {
            return ResponseEntity.ok("createMovie failed");
        }
    }
}
