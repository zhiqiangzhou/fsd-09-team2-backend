package com.webservice.service.impl;

import com.webservice.controller.MovieController;
import com.webservice.entity.Movie;
import com.webservice.repo.MovieRepo;
import com.webservice.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {


    @Autowired
    private MovieRepo movieRepo;

    @Override
    public List<Movie> getAllMovies() {
        return movieRepo.findAll();
    }

    @Override
    public Boolean createMovie(MovieController.MovieDTO movie) {
        Movie save = null;
        try {
            save = movieRepo.save(Movie.builder().
                    imageData(movie.getImage().getBytes()).
                    title(movie.getTitle()).
                    genre(movie.getGenre()).
                    year(movie.getYear()).
                    rating(movie.getRating()).
                    director(movie.getDirector()).
                    cast(movie.getCast()).
                    build());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return save.getId() != null;
    }

    @Override
    public Boolean deleteMovie(Long movieId) {
        movieRepo.deleteById(movieId);
        return true;
    }

    @Override
    public Boolean updateMovie(Long movieId, MovieController.MovieDTO movieDTO) {
        try {
            Optional<Movie> existingMovie = movieRepo.findById(movieId);
            if (existingMovie.isPresent()) {
                Movie movie = existingMovie.get();
                movie.setImageData(movieDTO.getImage().getBytes());
                movie.setTitle(movieDTO.getTitle());
                movie.setGenre(movieDTO.getGenre());
                movie.setYear(movieDTO.getYear());
                movie.setRating(movieDTO.getRating());
                movie.setDirector(movieDTO.getDirector());
                movie.setCast(movieDTO.getCast());
                return true;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return false;
    }
}
