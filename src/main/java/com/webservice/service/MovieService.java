package com.webservice.service;

import com.webservice.controller.MovieController;
import com.webservice.entity.Movie;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MovieService {
    List<Movie> getAllMovies();


    Boolean deleteMovie(Long movieId);

    Boolean updateMovie(Long movieId, MovieController.MovieDTO movieDTO);

    Boolean createMovie(MovieController.MovieDTO movie);
}
